# Jerry's Collection of configuration files for various applications.

This repository consists of configuration files for zsh, Alacritty, Conky, neofetch and more. Feel free to poke around. Maybe you find some things for your files! If you have suggestions, feel free to do a merge request.
